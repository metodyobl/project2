﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace MetObl2
{
   
    class Program
    {
        // maksymalna liczba kroków (pani mówiła że 100 styknie)
        private const int MaxSteps = 100; 
        // wbita na sztywno funkcja f(x) = x * x * x + x - 4
        public static double fS(double x)
        {
            return x * x * x + x - 4;
        }

        public static double fI(double x)
        {
            return Math.Pow((4 - x), 1/3.0);
        }

        //metoda liczaca Met siecznych zwraca klucz->wartosc x0 - > ilosc krokow
        public static KeyValuePair<double, double> MetodaSiecznych(double _a, double _b, double _e)
        {
            int steps = 0;
            double[] xInts = new double[MaxSteps];

            xInts[0] = _a;
            xInts[1] = _b;

            for (int i = 2; i < xInts.Length - 2; i++)
            {
                steps++;
                xInts[i] = Siecz(xInts[0], xInts[1]);
                if (Math.Abs(fS(xInts[i])) > _e)
                {
                    xInts[1] = xInts[i];
                }
                else
                {
                    return new KeyValuePair<double, double>(xInts[i], steps);
                }
            }
            Console.WriteLine("------------------------------");
            Console.WriteLine("Przekroczono ilosc iteracji (" +MaxSteps+ ") ");
            Console.WriteLine("------------------------------");
            return new KeyValuePair<double, double>(0, 0);

        }

        public static double Siecz(double _x1, double _x0)
        {
            return (_x1 - ((fS(_x1)*(_x1 - _x0))/(fS(_x1) - fS(_x0))));
        }


        //metoda liczaca Met siecznych zwraca klucz->wartosc x0 - > ilosc krokow
        public static KeyValuePair<double, double> MetodaIteracyjna(double _a, double _b, double _e)
        {
            int steps = 0;
            double[] xInts = new double[MaxSteps];
            xInts[0] = _a;

            for (int i = 1; i < MaxSteps - 2 ; i++)
            {
                steps++;
                xInts[i] = fI(xInts[i - 1]);
              //  Console.WriteLine(" x = " + xInts[i]);
                if (Math.Abs(xInts[i] - xInts[i - 1]) < _e)
                {
                    return new KeyValuePair<double, double>(xInts[i], steps);
                }
            }

            Console.WriteLine("------------------------------");
            Console.WriteLine("Przekroczono ilosc iteracji (" + MaxSteps + ") ");
            Console.WriteLine("------------------------------");
            return new KeyValuePair<double, double>(0, 0);
        }

        // glowny program
        static void Main(string[] args)
        {
            bool error = false;
            bool next = false;

            double a = 0;
            double b = 0;
            double e = 0;
            while (true)
            {
                Console.WriteLine(
                    "Obliczanie pierwiastka funkcji  \n f(x) = x * x * x + x - 4 \n metodą siecznych oraz metoda iteracyjną oraz porównanie liczby kroków");
                Console.WriteLine("------------------------------");
                
                while (!next)
                {
                    Console.WriteLine("Podaj poczatkową wartośc przedziału:");
                    next = double.TryParse(Console.ReadLine(), out a);
                    if(!next)
                        Console.WriteLine("Błędna wartość!");
                }
                next = false;

                while (!next)
                {
                    Console.WriteLine("Podaj końcową wartośc przedziału:");
                    next = double.TryParse(Console.ReadLine(), out b);
                    if (!next)
                        Console.WriteLine("Błędna wartość!");
                }
                next = false;

                while (!next)
                {
                    Console.WriteLine("Podaj przybliżenie:");
                    next = double.TryParse(Console.ReadLine(), out e);
                    if (!next)
                        Console.WriteLine("Błędna wartość!");
                }
                Console.WriteLine("------------------------------");
                next = false;

                if (a >= b)
                {
                    Console.WriteLine("Początkowa wartość przedziału nie może być większa od końcowej!");
                    error = true;
                }
                if (!error)
                {
                    double sieczna = MetodaSiecznych(a, b, e).Key;
                    double iteracyjna = MetodaIteracyjna(a, b, e).Key;
                    if (double.IsNaN(sieczna)) Console.WriteLine("METODA SIECZNYCH : wystąpiło dzielenie przez 0!");
                    else
                        Console.WriteLine("METODA SIECZNYCH : x =  " + sieczna + " Ilość kroków : " +
                                          MetodaSiecznych(a, b, e).Value);
                    if (double.IsNaN(iteracyjna)) Console.WriteLine("METODA ITERACYJNA : wystąpiło dzielenie przez 0!");
                    Console.WriteLine("METODA ITERACYJNA : x =  " + iteracyjna + " Ilość kroków : " +
                                      MetodaIteracyjna(a, b, e).Value);
                }
                error = false;
                Console.ReadKey();
                Console.Clear();
            }
        }

      

   
    }
}
